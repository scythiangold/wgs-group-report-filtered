Attribute VB_Name = "WGSGroupReportFiltered"
Sub Main()

    Dim GreaterThanTwoPercent As Worksheet
    Dim GreaterThanTwoPercentDiffTen As Worksheet
    Dim Summary As Worksheet
    Dim FilePath As String
    Dim WGSReportFileName As String
    Dim EndRow As Long
    Dim CurrentMonth As String
    Dim CurrentDate As Date
    Dim CurrentMonthNum As Integer
    Dim Counter As Long
    Dim FirstAnalyst As String
    Dim SecondAnalyst As String
    Dim MidRow As Double
    Dim Source As String
    Dim SourceName As String
    Dim RowCounter As Long
    Dim ColCounter As Integer
    Dim CurrentMonthCol As Integer
    Dim YearMonth() As String
    
    CurrentMonthCol = 0
    Source = vbNullString
    SourceName = vbNullString
    Variance = vbNullString
    VariancePercent = vbNullString
    
    Application.CutCopyMode = False
    Application.DisplayAlerts = False
    Application.ScreenUpdating = False
    Application.Calculation = xlCalculationManual
'    Application.ErrorCheckingOptions.BackgroundChecking = False
  
    With Workbooks("WGS Group Report Filtered.xlsm").Sheets("Sheet1")

        FirstAnalyst = ""
        SecondAnalyst = ""
        FilePath = .Range("B7").Value
        
    End With
    
'   Open correct sample template
    Application.Workbooks.Open FilePath & "Year3 Display_crosstab.csv", UpdateLinks:=False
    
    WGSReportFileName = "WGS_EDWard_GRP_RPT_MONTHLY_" & Format(Date, "yyyymmdd") & "_filtered.xlsx"
    
'   Save copy of sample template with correct filename
    Application.Workbooks(2).SaveAs FileName:=FilePath & WGSReportFileName, FileFormat:=xlOpenXMLWorkbook
    
    With Workbooks(WGSReportFileName)
        
        Set GreaterThanTwoPercent = .Sheets.Add(After:=Worksheets(Worksheets.Count))
        GreaterThanTwoPercent.Name = "> 2%"
        Set GreaterThanTwoPercentDiffTen = .Sheets.Add(After:=Worksheets(Worksheets.Count))
        GreaterThanTwoPercentDiffTen.Name = "> 2% DIFF > 10"
        Set Summary = .Sheets.Add(After:=Worksheets(Worksheets.Count))
        Summary.Name = "SUMMARY"
        
    End With
    
    With Workbooks(WGSReportFileName).Sheets("Year3 Display_crosstab")
    
        EndRow = .Cells(Rows.Count, 1).End(xlUp).Row
    
        For ColCounter = 19 To 8 Step -1
        
'            EndRow = .Cells(3, ColCounter).End(xlDown).Row
            For RowCounter = 3 To EndRow
            
                If .Cells(RowCounter, ColCounter).Value <> 0 _
                And .Cells(RowCounter, ColCounter).Value <> vbNullString Then
                
                    CurrentMonthCol = CStr(ColCounter)
                    Exit For
                    
                End If
                
            Next
            
            If CurrentMonthCol <> 0 Then Exit For
            
        Next
        
        YearMonth = Split(.Cells(2, CurrentMonthCol).Value)
        CurentMonth = YearMonth(2)
        CurrentDate = CDate(CurrentMonth & " 1, 2000")
        CurrentMonthNum = Month(CurrentDate)
        
    End With
    
    Application.Workbooks.Open FilePath & "Month_3_Full_Data_data.csv", UpdateLinks:=False
    
    If Workbooks(3).Worksheets("Month_3_Full_Data_data").Range("E2").Value = "SRC_ELIG to EDWard Compare" Then
    
        SourceName = "SRC_ELIG to EDWard Compare"
        Source = "SRC_ELIG"
        Variance = "Diff (SRC_ELIG - EDWard)"
        VariancePercent = "VAR%(SRC_ELIG - EDWard)"
        
    ElseIf Workbooks(3).Worksheets("Month_3_Full_Data_data").Range("E2").Value = "SRC_ELIG to RMMC Compare" Then
    
        SourceName = "SRC_ELIG to RMMC Compare"
        Source = "SRC_ELIG"
        Variance = "Diff (SRC_ELIG - RMMC)"
        VariancePercent = "VAR%(SRC_ELIG - RMMC)"
    
    ElseIf Workbooks(3).Worksheets("Month_3_Full_Data_data").Range("E2").Value = "SRC_ELIG to RMMC_EVA Compare" Then
    
        SourceName = "SRC_ELIG to RMMC_EVA Compare"
        Source = "SRC_ELIG"
        Variance = "Diff (SRC_ELIG - RMMC_EVA)"
        VariancePercent = "VAR%(SRC_ELIG - RMMC_EVA)"
    
    ElseIf Workbooks(3).Worksheets("Month_3_Full_Data_data").Range("E2").Value = "LGDW to RMMC Compare" Then
    
        SourceName = "LGDW to RMMC Compare"
        Source = "LGDW"
        Variance = "Diff (LGDW - RMMC)"
        VariancePercent = "VAR%(LGDW - RMMC)"
        
    End If
    
    If Workbooks(3).Worksheets("Month_3_Full_Data_data").Range("A2").Value = "NON CII" Then
    
        Workbooks(3).Worksheets("Month_3_Full_Data_data").Range(Range("I2"), Range("I2").End(xlDown)).Copy
        Workbooks(2).Worksheets("> 2%").Range("A1").PasteSpecial Paste:=xlPasteValues
        
        Application.Workbooks(3).Close
        
        Application.Workbooks.Open FilePath & "Month_3_Full_Data_data (1).csv", UpdateLinks:=False
        
        If Workbooks(3).Worksheets("Month_3_Full_Data_data (1)").Range("E2").Value <> SourceName Then
        
            MsgBox "CII and Non CII Source does not match"
            Exit Sub
            
        End If
        
        Workbooks(3).Worksheets("Month_3_Full_Data_data (1)").Range(Range("I2"), Range("I2").End(xlDown)).Copy
        EndRow = Workbooks(2).Worksheets("> 2%").Cells(Rows.Count, "A").End(xlUp).Row + 1
        Workbooks(2).Worksheets("> 2%").Range("A" & EndRow).PasteSpecial
        
        Application.Workbooks(3).Close
        
    ElseIf Workbooks(3).Worksheets("Month_3_Full_Data_data").Range("A2").Value = "CII" Then
    
        Application.Workbooks(3).Close
        
        Application.Workbooks.Open FilePath & "Month_3_Full_Data_data (1).csv", UpdateLinks:=False
        
        If Workbooks(3).Worksheets("Month_3_Full_Data_data (1)").Range("E2").Value <> SourceName Then
        
            MsgBox "CII and Non CII Source does not match"
            Exit Sub

        End If
    
        Workbooks(3).Worksheets("Month_3_Full_Data_data (1)").Range(Range("I2"), Range("I2").End(xlDown)).Copy
        Workbooks(2).Worksheets("> 2%").Range("A1").PasteSpecial Paste:=xlPasteValues
        
        Application.Workbooks(3).Close
        
        Application.Workbooks.Open FilePath & "Month_3_Full_Data_data.csv", UpdateLinks:=False
        
        Workbooks(3).Worksheets("Month_3_Full_Data_data").Range(Range("I2"), Range("I2").End(xlDown)).Copy
        EndRow = Workbooks(2).Worksheets("> 2%").Cells(Rows.Count, "A").End(xlUp).Row + 1
        Workbooks(2).Worksheets("> 2%").Range("A" & EndRow).PasteSpecial
        
        Application.Workbooks(3).Close
        
    End If
    
    With Workbooks(WGSReportFileName).Sheets("> 2%")
    
        .Activate
        .Range("A1").Select
    
        EndRow = .Range("A1").End(xlDown).Row
'        EndRow = .Cells(Rows.Count, "A").End(xlUp).Row
        .Range(Range("A1"), Range("A" & EndRow)).TextToColumns DataType:=xlFixedWidth
        .Range(Range("A1"), Range("A" & EndRow)).NumberFormat = "@"
    
        For Counter = 1 To EndRow
        
            If IsNumeric(.Cells(Counter, 1).Value) = True Then
            
                .Cells(Counter, 1).Value = Format(VBA.Trim(.Cells(Counter, 1).Value), "000000")
                .Cells(Counter, 1).Errors(3).Ignore = True
                
            End If
            
        Next
        
    End With
    
    With Workbooks(WGSReportFileName).Sheets("Year3 Display_crosstab")
    
'        EndRow = .Range("C2").End(xlDown).Row
        EndRow = .Cells(Rows.Count, "C").End(xlUp).Row
        .Range(.Range("C3"), .Range("C" & EndRow)).TextToColumns DataType:=xlFixedWidth
        .Range(.Range("C3"), .Range("C" & EndRow)).NumberFormat = "@"
        
        For Counter = 3 To EndRow
        
            If IsNumeric(.Cells(Counter, 3).Value) = True Then
            
                .Cells(Counter, 3).Value = Format(VBA.Trim(.Cells(Counter, 3).Value), "000000")
                .Cells(Counter, 3).Errors(3).Ignore = True
                
            End If
            
        Next
    
        .Range("U1:U2").Clear
        .Range("U2") = "over 2%"
        .Range("V2") = "over 2% > 10"
        
        EndRow = .Range("A3").End(xlDown).Row
        .Range(.Range("U3"), .Range("U" & EndRow)).Formula = "=VLOOKUP(C3,'> 2%'!A:A,1,FALSE)"
        .Range(.Range("U3"), .Range("U" & EndRow)).TextToColumns DataType:=xlFixedWidth
        .Range(.Range("U3"), .Range("U" & EndRow)).NumberFormat = "@"
        
        For Counter = 3 To EndRow
        
            If IsNumeric(.Cells(Counter, 3).Value) = True Then
            
                .Cells(Counter, 3).Value = Format(VBA.Trim(.Cells(Counter, 3).Value), "000000")
                .Cells(Counter, 3).Errors(3).Ignore = True
                
            End If
            
        Next

        .Range(.Range("A2"), .Range("U2").End(xlDown)).AutoFilter Field:=21, Criteria1:="<>#N/A"
        .Range(.Range("A2"), .Range("U2").End(xlDown)).AutoFilter Field:=7, Criteria1:=Variance
        .Range(.Range("A2"), .Range("U2").End(xlDown)).AutoFilter Field:=CurrentMonthCol, Criteria1:=">10", Operator:=xlOr, Criteria2:="<-10"
        
        .Range(.Range("U3"), .Range("U3").End(xlDown)).Copy
        
    End With
    
    With Workbooks(WGSReportFileName).Sheets("> 2% DIFF > 10")
    
        .Range("A1").PasteSpecial Paste:=xlPasteValues
        .AutoFilterMode = False
        
        .Activate
        .Range("A1").Select
        
        EndRow = Workbooks(2).Sheets("> 2% DIFF > 10").Cells(Rows.Count, "A").End(xlUp).Row
        .Range(.Range("A1"), .Range("A" & EndRow)).TextToColumns DataType:=xlFixedWidth
        .Range(.Range("A1"), .Range("A" & EndRow)).NumberFormat = "@"

        For Counter = 1 To EndRow

            If IsNumeric(.Cells(Counter, 1).Value) = True Then
            
                .Cells(Counter, 1).Value = Format(VBA.Trim(.Cells(Counter, 1).Value), "000000")
                .Cells(Counter, 1).Errors(3).Ignore = True
            
            End If

        Next
        
    End With
    
    Application.Workbooks(2).Save
'   FileName:=FilePath & WGSReportFileName, FileFormat:=xlOpenXMLWorkbook
    
    With Workbooks(WGSReportFileName).Sheets("Year3 Display_crosstab")
    
        .AutoFilterMode = False
        EndRow = .Range("V3").End(xlDown).Row
        .Range(.Range("V3"), .Range("V" & EndRow)).Formula = "=VLOOKUP(C3,'> 2% DIFF > 10'!A:A,1,FALSE)"
        .Range(.Range("V3"), .Range("V" & EndRow)).TextToColumns DataType:=xlFixedWidth
        .Range(.Range("V3"), .Range("V" & EndRow)).NumberFormat = "@"
        
        For Counter = 3 To EndRow
        
            If IsNumeric(.Cells(Counter, 3).Value) = True Then
            
                .Cells(Counter, 3).Value = Format(VBA.Trim(.Cells(Counter, 3).Value), "000000")
                .Cells(Counter, 3).Errors(3).Ignore = True
                
            End If
                
        Next
        
        .Range(.Range("A2"), .Range("V2").End(xlDown)).AutoFilter Field:=22, Criteria1:="#N/A"

        EndRow = .Range("V3").End(xlDown).Row
       .Rows("3:" & EndRow).EntireRow.Delete

        .AutoFilterMode = False
        .Range(.Range("A2"), .Range("V2").End(xlDown)).AutoFilter Field:=7, Criteria1:=Variance
        
        .Range(.Range("B2"), .Range("D2").End(xlDown)).Copy
        
    End With
    
    With Workbooks(WGSReportFileName).Sheets("SUMMARY")
    
        .Range("B3").PasteSpecial Paste:=xlPasteValues
        .AutoFilterMode = False
        EndRow = .Range("C3").End(xlDown).Row
        .Range(.Range("C3"), .Range("C" & EndRow)).TextToColumns DataType:=xlFixedWidth
        .Range(.Range("C3"), .Range("C" & EndRow)).NumberFormat = "@"
        
    End With
    
    With Workbooks(WGSReportFileName).Sheets("Year3 Display_crosstab")
    
        .Range(.Cells(3, CurrentMonthCol), .Cells(3, CurrentMonthCol).End(xlDown)).Copy
        
    End With
    
    With Workbooks(WGSReportFileName).Sheets("SUMMARY")
    
        .Range("E4").PasteSpecial Paste:=xlPasteValues
        
    End With
    
    With Workbooks(WGSReportFileName).Sheets("Year3 Display_crosstab")
    
        .AutoFilterMode = False
        .Range(.Range("A2"), .Range("V2").End(xlDown)).AutoFilter Field:=7, Criteria1:=VariancePercent
    
        .Range(.Cells(3, CurrentMonthCol), .Cells(3, CurrentMonthCol).End(xlDown)).Copy
        
    End With
    
    With Workbooks(WGSReportFileName).Sheets("SUMMARY")
    
        .Range("F4").PasteSpecial Paste:=xlPasteValues
        
        .Range("C3").Value = "Purchaser Organization Number"
        .Range("D3").Value = "Purchaser Organization Name"
        .Range("E3").Value = "Variance"
        .Range("F3").Value = "Percent of Variance"
        .Range("G3").Value = "Evaluation"
        .Range("B3:G3").Font.Bold = True
        .Range("B3:G3").Interior.Color = RGB(231, 230, 230)
        
        .Range(.Range("C3"), .Range("C" & EndRow)).TextToColumns DataType:=xlFixedWidth
        
        EndRow = .Range("B3").End(xlDown).Row
        
        For Counter = EndRow To 4 Step -1
        
            If IsNumeric(.Cells(Counter, 3).Value) = True Then
             
                .Cells(Counter, 3).Value = Format(VBA.Trim(.Cells(Counter, 3).Value), "000000")
                .Cells(Counter, 3).Errors(3).Ignore = True
                
            End If
            
            If .Cells(Counter, 4).Value = "TEREX CORPORATION" _
            Or .Cells(Counter, 4).Value = "NEXT LEVEL RESOURCE PARTNERS LLC" _
            Or .Cells(Counter, 4).Value = "GOOGLE INC" _
            Or .Cells(Counter, 4).Value = "CB INFORMATION SERVICES, INC." _
            Then .Rows(Counter).EntireRow.Delete
            
        Next
        
        EndRow = .Range("B3").End(xlDown).Row
        
        Set SummaryTable = .Range(.Range("B3"), .Range("G" & EndRow))
        
        If Source = "SRC_ELIG" Then
        
            MidRow = Application.RoundUp((EndRow - 4) / 2, 0)
            
        ElseIf Source = "LGDW" Then
        
            MidRow = Application.Round((EndRow - 4) / 2, 0)
        
        End If
        
        If FirstAnalyst <> "" And SecondAnalyst <> "" Then
        
            For Counter = 4 To MidRow + 4
            
                .Cells(Counter, 7).Value = "(" & FirstAnalyst & ")"
                
            Next
            
            For Counter = MidRow + 4 To EndRow
            
                .Cells(Counter, 7).Value = "(" & SecondAnalyst & ")"
                
            Next
            
        End If
        
        .Select
        .Columns("B:G").AutoFit
        
        SummaryTable.Borders.Weight = xlThin
        
        SummaryTable.Borders(xlEdgeBottom).LineStyle = xlContinuous
        SummaryTable.Borders(xlEdgeLeft).LineStyle = xlContinuous
        SummaryTable.Borders(xlEdgeRight).LineStyle = xlContinuous
        SummaryTable.Borders(xlEdgeTop).LineStyle = xlContinuous
        SummaryTable.Borders(xlEdgeBottom).Weight = xlMedium
        SummaryTable.Borders(xlEdgeLeft).Weight = xlMedium
        SummaryTable.Borders(xlEdgeRight).Weight = xlMedium
        SummaryTable.Borders(xlEdgeTop).Weight = xlMedium
        
        .Range("B" & EndRow + 2).Value = "The following members groups have known LGDW issues and have been filtered from the list (if included):"
        .Range("B" & EndRow + 3).Value = "TEREX CORPORATION (270020)"
        .Range("B" & EndRow + 4).Value = "NEXT LEVEL RESOURCE PARTNERS LLC (CII group - 195617)"
        .Range("B" & EndRow + 5).Value = "GOOGLE INC (300188)"
        .Range("B" & EndRow + 6).Value = "CB INFORMATION SERVICES, INC. (DIM465231, JIRAD WGS-29334)"
        
    End With
    
    With Workbooks(WGSReportFileName).Sheets("Year3 Display_crosstab")
    
        .AutoFilterMode = False
        
        .Columns("H:S").Hidden = True
        .Columns(CurrentMonthCol).Hidden = False
        
        .Range(.Range("A2"), .Range("V2").End(xlDown)).AutoFilter Field:=7, Criteria1:=Variance
        
        .Select
        .Columns("A:G").AutoFit
        .Columns(CurrentMonthCol).AutoFit
        .Columns("T:V").AutoFit
        
    End With
    
    With Workbooks(WGSReportFileName)
    
        For Counter = 1 To Sheets.Count
        
            .Sheets(Counter).Activate
            .Sheets(Counter).Range("A1").Select
            
        Next
        
    End With
    
    Application.Workbooks(2).Save
    
    Application.Workbooks(2).Close
    
    Application.CutCopyMode = True
    Application.DisplayAlerts = True
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
'    Application.ErrorCheckingOptions.BackgroundChecking = True

End Sub
